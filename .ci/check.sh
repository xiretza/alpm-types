#!/bin/bash
#
# SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
# SPDX-License-Identifier: LGPL-3.0-or-later

set -euxo pipefail

cargo fmt -- --check
cargo clippy --all -- -D warnings
cargo deny check
cargo test --all
codespell --skip '.cargo,.git,target' --ignore-words-list 'crate'
reuse lint
